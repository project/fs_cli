<?php

namespace Drupal\fs_cli\Commands;

use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Provides some helper tools to interact with drupal file-system.
 */
class FsCliCommands extends DrushCommands {

  /**
   * S3fsToCliCommands constructor.
   */
  public function __construct(
    private readonly FileSystemInterface $fileSystem,
    private readonly SerializerInterface $serializer
  ) {
    parent::__construct();
  }

  /**
   * Check if a directory exists.
   *
   * @param string $directory
   *   The directory to check.
   * @param string[] $options
   *   The options to apply to the directory check.
   *
   * @command fs:directory-exists
   * @aliases fs-de
   */
  public function fsDirectoryExists(string $directory, array $options = ['format' => 'json']) {
    return $this->serializer->serialize(['is_dir' => is_dir($directory)], $options['format']);
  }

  /**
   * Check if a file exists.
   *
   * @param string $file
   *   The file to check.
   * @param string[] $options
   *   The options to apply to the file check.
   *
   * @command fs:file-exists
   * @aliases fs-fe
   */
  public function fsFileExists(string $file, array $options = ['format' => 'json']) {
    return $this->serializer->serialize(['file_exists' => file_exists($file)], $options['format']);
  }

  /**
   * Moves a file to a new location.
   *
   * @param string $file
   *   The file to move.
   * @param string $destination
   *   The destination to move the file to.
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *    - FileSystemInterface::EXISTS_REPLACE - Replace the existing file.
   *    - FileSystemInterface::EXISTS_RENAME - Append _{incrementing number}
   *      until the filename is unique.
   *    - FileSystemInterface::EXISTS_ERROR - Do nothing and return FALSE.
   * @param array $options
   *   The options to apply to the file move.
   *
   * @command fs:move
   */
  public function fsMove(string $file, string $destination, int $replace = FileSystemInterface::EXISTS_REPLACE, array $options = ['format' => 'json']) {
    try {
      $dir = $this->fileSystem->dirname($destination);
      $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
      $destination = $this->fileSystem->move($file, $destination, $replace);
    }
    catch (\Exception $e) {
      return $this->serializer->serialize(['error' => $e->getMessage()], $options['format']);
    }
    return $this->serializer->serialize(['success' => "File $file moved to $destination"], $options['format']);
  }

  /**
   * Copy a file to a new location.
   *
   * @param string $source
   *   The file to copy.
   * @param string $destination
   *   The destination to copy the file to.
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *     - FileSystemInterface::EXISTS_REPLACE - Replace the existing file.
   *     - FileSystemInterface::EXISTS_RENAME - Append _{incrementing number}
   *       until the filename is unique.
   *     - FileSystemInterface::EXISTS_ERROR - Do nothing and return FALSE.
   * @param array $options
   *   The options to apply to the file copy.
   *
   * @command fs:copy
   */
  public function fsCopy(string $source, string $destination, int $replace = FileSystemInterface::EXISTS_REPLACE, array $options = ['format' => 'json']) {
    try {
      $dir = $this->fileSystem->dirname($destination);
      $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
      $destination = $this->fileSystem->copy($source, $destination, $replace);
    }
    catch (\Exception $e) {
      return $this->serializer->serialize(['error' => $e->getMessage()], $options['format']);
    }

    return $this->serializer->serialize(['success' => "File $source copied to $destination"], $options['format']);
  }

  /**
   * Scan a directory for files.
   *
   * @param string $directory
   *   The directory to scan.
   * @param string $mask
   *   The mask to apply to the scan.
   * @param string[] $options
   *   The options to apply to the scan.
   *
   * @command fs:scan-dir
   */
  public function fsScanDir(string $directory, string $mask, array $options = ['format' => 'json']) {
    try {
      $files = $this->fileSystem->scanDirectory($directory, $mask);
    }
    catch (\Exception $e) {
      return $this->serializer->serialize(['error' => $e->getMessage()], $options['format']);
    }

    return $this->serializer->serialize(['files' => array_keys($files)], $options['format']);
  }

  /**
   * Create a directory.
   *
   * @param string $directory
   *   The directory to create.
   * @param string[] $options
   *   The options to apply to the directory creation.
   *
   * @command fs:mkdir
   */
  public function fsCreateDir(string $directory, array $options = ['format' => 'json']) {
    try {
      $this->fileSystem->mkdir($directory);
    }
    catch (\Exception $e) {
      return $this->serializer->serialize(['error' => $e->getMessage()], $options['format']);
    }
    return $this->serializer->serialize(['success' => "Directory $directory created"], $options['format']);
  }

  /**
   * Delete a directory.
   *
   * @param string $directory
   *   The directory to delete.
   * @param string[] $options
   *   The options to apply to the directory deletion.
   *
   * @command fs:delete-dir
   */
  public function fsDeleteDir(string $directory, array $options = ['format' => 'json']) {
    try {
      $this->fileSystem->deleteRecursive($directory);
    }
    catch (\Exception $e) {
      return $this->serializer->serialize(['error' => $e->getMessage()], $options['format']);
    }

    return $this->serializer->serialize(['success' => "Directory $directory deleted"], $options['format']);
  }

}
